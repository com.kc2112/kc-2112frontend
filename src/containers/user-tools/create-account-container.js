import React, {Component} from 'react';
import {connect} from "react-redux";
import {accountActions} from '../../redux/actions/login-actions';
import UserCreateAccount from "../../components/user-tools/user-create-account";
import {CANCEL_CREATE_ACCOUNT, SUBMIT_CREATE_ACCOUNT_DATA} from "../../redux/types";
class CreateAccountContainer extends Component {
constructor(props) {
    super(props);
    this.handleCancel = this.handleCancel.bind(this);
}
    handleSubmit(values) {
        console.log(values);
        const {accountActions} = this.props;
        accountActions(SUBMIT_CREATE_ACCOUNT_DATA, values);
    }

    handleCancel() {
        const {accountActions} = this.props;
        accountActions(CANCEL_CREATE_ACCOUNT);
    }

    render() {
        return (
            <UserCreateAccount onSubmit={this.handleSubmit} onCancelCreate={this.handleCancel}/>
        );
    }
}

const mapDispatchToProps = {
    accountActions
}

export default connect(null, mapDispatchToProps)(CreateAccountContainer);