import React, {Component} from 'react';
import LoginForm from "../../components/user-tools/user-login";
import {connect} from "react-redux";
import {loginSubmitAction} from '../../redux/actions/login-actions';

class LoginContainer extends Component {
    constructor(props) {
        super(props);
        this.handleCreateNew = this.handleCreateNew.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(values) {
        const {loginSubmitAction} = this.props;
        loginSubmitAction(values);
    }

    handleCreateNew(event) {
        event.preventDefault();
    }

    render() {
        const {loginStatus} = this.props;
        return (
          <LoginForm
              onSubmit={this.handleSubmit}
              handleCreateNew={this.handleCreateNew}
              loginStatus={loginStatus}
          />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginStatus: state.loginStatus
    };
}

const mapDispatchToProps = {
    loginSubmitAction
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);