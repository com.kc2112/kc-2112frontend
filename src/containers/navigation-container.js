import React, {Component} from 'react';
import {connect} from "react-redux";
import {loginStatusSelector} from "../redux/selectors/login-selector";
import {STATUS} from "../utils/constants";
import AppHeader from '../components/app-header';
import LoginContainer from "./user-tools/login-container";


class NavigationContainer extends Component {

    getRenderComponent() {
        const {loginStatus} = this.props;

        switch(loginStatus) {
            case STATUS.NONE:
            case STATUS.PENDING:
            case STATUS.FAILURE:
                 return <LoginContainer />;
            default:
                return <AppHeader />
        }
    }

    render() {
        return (
            <div>
                {this.getRenderComponent()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginStatus: loginStatusSelector(state),
        accountStatus: state.accountStatus
    };
}

export default connect(mapStateToProps)(NavigationContainer);
