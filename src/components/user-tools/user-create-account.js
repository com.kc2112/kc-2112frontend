import React, {Component} from 'react';
import {Field, reduxForm} from "redux-form";
import './user-create-account.css';

class UserCreateAccount extends Component {

    render() {

        const {handleSubmit, pristine, submitting, onCancelCreate} = this.props;

        return (
            <form onSubmit={handleSubmit} className="user-create-account-wrapper">
                <div className="user-create-account-header">Create New Account</div>
                <div className="user-create-account-field-container">
                    <label htmlFor="userName">UserName</label>
                    <Field name="userName" component="input" type="text"/>
                </div>
                <div className="user-create-account-field-container">
                    <label htmlFor="PassPhrase">Pass Phrase</label>
                    <Field name="PassPhrase" component="input" type="password"/>
                </div>
                <div className="user-create-account-field-container">
                    <label htmlFor="PassPhrase">Pass Phrase again</label>
                    <Field name="PassPhrase2" component="input" type="password"/>
                </div>
                <div className="user-create-account-button-container">
                    <button
                        className="user-create-account-submit-button"
                        type="submit"
                        disabled={pristine || submitting}
                    >Submit
                    </button>
                    <button
                        className="user-create-account-submit-button"
                        type="button"
                        onClick={onCancelCreate}
                        disabled={submitting}
                    >Cancel
                    </button>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: 'createNewUser'
})(UserCreateAccount);