import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form'
import './user-login.css';
import {FORM_NAMES, STATUS} from "../../utils/constants";

class LoginForm extends Component {

    render() {

        const { handleSubmit, pristine, submitting, resetForm, handleCreateNew, loginStatus} = this.props;

        return (
            <form onSubmit={handleSubmit} className="user-login-wrapper">
                <div className="user-login-header">header</div>
                {
                    loginStatus === STATUS.FAILURE &&
                        <div className="user-login-failure-wrapper">login failure</div>
                }
                <div className="user-login-field-container">
                    <label htmlFor="userName">UserName</label>
                    <Field name="userName" component="input" type="text" />
                </div>
                <div className="user-login-field-container">
                    <label htmlFor="password">Pass Phrase</label>
                    <Field name="password" component="input" type="password" />
                </div>
                <div className="user-login-button-container">
                <button
                    className="user-login-submit-button"
                    type="submit"
                    disabled={pristine || submitting}
                >Submit</button>
                </div>
                <div className="user-login-create-profile-container">
                    <a href="" onClick={handleCreateNew}>create a profile</a>
                </div>

            </form>
        );
    }
}

export default reduxForm({
    form: FORM_NAMES.LOGIN_FORM
})(LoginForm);