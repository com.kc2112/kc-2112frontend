import React from 'react';

import settingsIcon from '../../assets/gear.png';
import './user-control.css';

export default (props) => {
    return (
      <div className='user-control-wrapper'>
          <img className="user-control-settings-img" src={settingsIcon} alt="Settings" />
        UserControl
      </div>
    );
}