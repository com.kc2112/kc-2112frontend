import React from 'react';

import {HashRouter as Router, Switch, Route, Link} from 'react-router-dom';
import UserControl from './user-tools/user-control';

import '../css/components/router.css';

export default (props) => {
    return (
        <Router>
            <nav className="router-nav">
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                    <li>
                        <Link to="/users">Users</Link>
                    </li>
                    <li><UserControl /></li>
                </ul>
                <Switch>
                    <Route path="/about">
                        {(props) => <div>About</div>}
                    </Route>
                    <Route path="/users">
                        {(props) => <div>Users</div>}
                    </Route>
                    <Route path="/">
                        {(props) => <div>Home</div>}
                    </Route>
                </Switch>
            </nav>
        </Router>
    );
}