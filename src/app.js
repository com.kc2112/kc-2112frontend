import React from "react";
import {Helmet} from "react-helmet";
import Nav from './containers/navigation-container';

import './app.css';

const App = () => {
    return (
        <div className="application-wrapper">
            <Helmet>
                <title>Kc-2112</title>
                <meta charSet="utf-8" />
                <meta http-equiv="Pragma" content="no-cache" />
                <meta http-equiv="Expires" content="0" />
            </Helmet>
            <Nav />
        </div>
    );
};

export default App;