import axios from 'axios';
import {reset} from 'redux-form';
import {LOGIN_SUBMIT_DATA, LOGIN_STATUS, UPDATE_USER_DATA} from "../types";
import {BASE_URL, FORM_NAMES, STATUS} from "../../utils/constants";

export default store => next => action => {

    if (action.type !== LOGIN_SUBMIT_DATA) {
        return next(action);
    }

    const {dispatch} = store;

    dispatch({
        type: LOGIN_STATUS,
        payload: STATUS.PENDING
    });
    const {payload} = action;

    const data = {
        userName: btoa(payload.userName),
        password: btoa(payload.password)
    };
    axios.post(`${BASE_URL}/login`, data).then(response => {

        const {loginStatus, uuid} = response.data;

        dispatch({
            type: LOGIN_STATUS,
            payload: loginStatus
        });

        dispatch({
            type: UPDATE_USER_DATA,
            payload: loginStatus === STATUS.SUCCESS ? {uuid} : null
        });


    }).catch(error => {
        dispatch({
            type: LOGIN_STATUS,
            payload: STATUS.FAILURE
        });
        dispatch({
            type: UPDATE_USER_DATA,
            payload: null
        });

        dispatch(reset(FORM_NAMES.LOGIN_FORM));
    })

    return next(action);
}

const submitLoginRequest = (dispatch, data) => {
    return new Promise(() => {
        return ;
    });
}