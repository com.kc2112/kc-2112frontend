import {LOGIN_SUBMIT_DATA} from '../types';

export const loginSubmitAction = (payload) => {
    return {
        type: LOGIN_SUBMIT_DATA,
        payload
    };
}
