import {LOGIN_SUBMIT_DATA, LOGIN_STATUS} from '../types';
import {STATUS} from "../../utils/constants";

const DEFAULT_STATE = STATUS.NONE;

export default (state = DEFAULT_STATE, action) => {

    switch(action.type) {
        case LOGIN_STATUS:
            return action.payload;
        default:
            return state;
    }
}

