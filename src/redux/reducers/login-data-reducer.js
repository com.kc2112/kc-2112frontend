import {LOGIN_STATUS} from '../types';

const DEFAULT_STATE = null;

export default (state = DEFAULT_STATE, action) => {

    switch(action.type) {
        case LOGIN_STATUS:
            return action.payload;
        default:
            return state;
    }
}