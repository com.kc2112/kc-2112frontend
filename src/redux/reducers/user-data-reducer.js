import {UPDATE_USER_DATA} from '../types';

const DEFAULT_STATE = null;

export default (state = DEFAULT_STATE, action) => {

    switch(action.type) {
        case UPDATE_USER_DATA:
            return action.payload;
        default:
            return state;
    }
}

