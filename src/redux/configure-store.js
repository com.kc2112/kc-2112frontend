import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { reducer as formReducer } from 'redux-form'
import userLoginMiddleware from './middleware/user-login-middleware';
import loginStatusReducer from './reducers/login-status-reducer';
import userDataReducer from './reducers/user-data-reducer';

const reducers = combineReducers({
    loginStatus: loginStatusReducer,
    userData: userDataReducer,
    form: formReducer
});

export default function configureStore(preloadedState) {

    const middlewares = [userLoginMiddleware, thunkMiddleware]
    const middlewareEnhancer = applyMiddleware(...middlewares)

    const enhancers = [middlewareEnhancer]
    const composedEnhancers = composeWithDevTools(...enhancers)

    return createStore(reducers, preloadedState, composedEnhancers)

}