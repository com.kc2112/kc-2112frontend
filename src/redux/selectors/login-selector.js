import { createSelector } from 'reselect'
import {MAX_LOGIN_ATTEMPTS, STATUS} from "../../utils/constants";

const loginStatusState = state => state.loginStatus;

export const loginStatusSelector = createSelector(
    [loginStatusState], (loginStatus) => {
       return loginStatus;
    });