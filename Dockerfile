# The image is built on top of one that has node preinstalled
FROM node:latest
# Create app directory
WORKDIR /usr/src/app
# Copy all files into the container
COPY . .
# Install dependencies
RUN npm install
# Open appropriate port
EXPOSE 80
# Start the application
CMD [ "npm", "dev" ]